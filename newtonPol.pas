unit newtonPol;

{$mode objfpc}{$H+}

interface

uses
   math;

type
   TArrofArrReal = array of array of real;

   TNewtonPol = class
   private
      pointsXY : TArrofArrReal;
      dividedDiff : array of real;
      ddOldData : array of real;
      n : integer;
   public
      constructor create();

      function basisPol(i : integer; x : real) : real;
      function eval (x : real) : real;
      procedure setPointsXY(ptsXY : TArrofArrReal);
      procedure addValue (x , fx : real);
   end;

implementation

constructor TNewtonPol.create();
begin
   setLength(pointsXY, 0, 0);
   setLength(dividedDiff, 0);
   n := 0;
end;

procedure TNewtonPol.setPointsXY(ptsXY : TArrofArrReal);
var
   i, j, cnt: integer;
begin
   pointsXY := ptsXY;
   n := length(ptsXY);

   setLength(ddOldData, n);
   setLength(dividedDiff, n);
   for i := 0 to n - 1 do
      ddOldData[i] := pointsXY[i][1];

   i := n - 2;
   cnt := 1;
   dividedDiff[0] := ddOldData[0];
   while (i >= 0) do
   begin
      for j := 0 to i do
         ddOldData[j] := (ddOldData[j + 1] - ddOldData[j]) / (pointsXY[j + cnt][0] - pointsXY[j][0]);
      dividedDiff[n - 1 - i] := ddOldData[0];
      cnt := cnt + 1;
      i := i - 1;

   end;
end;

function TNewtonPol.basisPol(i : integer; x : real) : real;
var
   j : integer;
   res : real;
begin
   if (i = 0) then
      exit(1);

   res := 1;
   for j := 0 to i - 1 do
      res := res * (x - pointsXY[j][0]);
   Result := res;
end;

function TNewtonPol.eval (x : real) : real;
var
   i : integer;
   res : real;
begin
   res := 0;
   for i := 0 to n - 1 do
      res := res + dividedDiff[i]* self.basisPol(i, x);
   Result := res;
end;

procedure TNewtonPol.addValue(x , fx : real);
var
   i : integer;
begin
   n := n + 1;
   setLength(pointsXY, n ,2);
   setLength(ddOldData, n);
   setLength(dividedDiff, n);

   pointsXY[n - 1][0] := x;
   pointsXY[n - 1][0] := fx;

   ddOldData[n - 1] := fx;
   i := n - 2;
   while (i >= 0) do
   begin
      ddOldData[i] := (ddOldData[i + 1] - ddOldData[i]) /  (pointsXY[n - 1][0] - pointsXY[i][0]);
      i := i - 1;
   end;
   dividedDiff[n - 1] := ddOldData[0];
end;
end.
