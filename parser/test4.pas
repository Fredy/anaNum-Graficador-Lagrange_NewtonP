program pepsd;

{$mode objfpc}{$H+}

uses
   gmap,  dialogs, gutil, mFunction, math, SysUtils, classes, mMaps, mParser;

function algo (const args : array of real) : real;
var
   i : integer;
   res : real;
begin
   res := 0;
   for i := 0 to length(args) do
   begin
      res :=  res + args[i];
   end;
   Result := res;
end;

var
   functions : TFunctionMap;
   constants : TVarMap;
   pparser : TParser;
   list : TStringList;
   str : string;
begin

   functions := TFunctionMap.create;
   constants := TVarMap.create;

   functions['fsin'] := TFunc.create(@algo, 3);
   functions['fcos'] := TFunc.create(@algo, 3);
   functions['fln'] := TFunc.create(@algo, 3);

   constants['cpi'] := pi;

   pparser := TParser.create(@(functions), @(constants));
   str := '((2+3^2)^2-(5*2)^2)/((1+2)^2-(3-1)^2)-0.2';
   list := pparser.parseString(str);
   list := pparser.toRpn(list);
   writeln(list.text);
end.
