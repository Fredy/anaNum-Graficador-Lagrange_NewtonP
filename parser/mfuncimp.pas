unit mFuncImp;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, math;

// Básicas : +, -, *, /, ^, negación.
function fsum(const args : array of real) : real;
function fsub(const args : array of real) : real;
function fmul(const args : array of real) : real;
function fdiv(const args : array of real) : real;
function fpow(const args : array of real) : real;
function fneg(const args : array of real) : real;

// ln, exp, log(base, número)
function fln(const args : array of real) : real;
function fexp(const args : array of real) : real;
function flog(const args : array of real) : real;

// trigonométricas sin, cos, tan, cot, sec, csc
function fsin(const args : array of real) : real;
function fcos(const args : array of real) : real;
function ftan(const args : array of real) : real;
function fctg(const args : array of real) : real;
function fsec(const args : array of real) : real;
function fcsc(const args : array of real) : real;

// trigonométricas inversas: arcoseno, arcocoseno, arcotangente
function fasin(const args : array of real) : real;
function facos(const args : array of real) : real;
function fatan(const args : array of real) : real;

// funciones hiperbólicas: sinh, cosh, tanh
function fsinh(const args : array of real) : real;
function fcosh(const args : array of real) : real;
function ftanh(const args : array of real) : real;

// inversas de funciones hiperbólicas: asinh, acosh, atanh
function fasinh(const args : array of real) : real;
function facosh(const args : array of real) : real;
function fatanh(const args : array of real) : real;

function ffact(const args : array of real) : real;
function fabs(const args : array of real) : real;
function ffloor(const args : array of real) : real;
function fceil(const args : array of real) : real;
function fsqrt(const args : array of real) : real;

implementation


function fsum(const args : array of real) : real;
begin
  Result := args[0] + args[1] ;
end;

function fsub(const args : array of real) : real;
begin
  Result := args[0] - args[1]  ;
end;

function fmul(const args : array of real) : real;
begin
  Result := args[0] * args[1]  ;
end;
function fdiv(const args : array of real) : real;
begin
  Result := args[0] / args[1]  ;
end;

function fpow(const args : array of real) : real;
begin
   Result := power(args[0], args[1]);
end;

function fneg(const args : array of real) : real;
begin
   Result := -args[0];
end;

function fln(const args : array of real) : real;
begin
   Result := ln(args[0]);
end;

function fexp(const args : array of real) : real;
begin
   Result := exp(args[0]);
end;

function flog(const args : array of real) : real;
begin
   Result := logn(args[0], args[1]);
end;

function fsin(const args : array of real) : real;
begin
   Result := sin(args[0]);
end;

function fcos(const args : array of real) : real;
begin
   Result := cos(args[0]);
end;


function ftan(const args : array of real) : real;
begin
   Result := tan(args[0]);
end;

function fctg(const args : array of real) : real;
begin
   Result := cotan(args[0]);
end;

function fsec(const args : array of real) : real;
begin
   Result := sec(args[0]);
end;

function fcsc(const args : array of real) : real;
begin
   Result := cosecant(args[0]);
end;

function fasin(const args : array of real) : real;
begin
   Result := arcsin(args[0]);
end;

function facos(const args : array of real) : real;
begin
   Result := arccos(args[0]);
end;

function fatan(const args : array of real) : real;
begin
   Result := arctan(args[0]);
end;

function fsinh(const args : array of real) : real;
begin
   Result := sinh(args[0]);
end;

function fcosh(const args : array of real) : real;
begin
   Result := cosh(args[0]);
end;

function ftanh(const args : array of real) : real;
begin
   Result := tanh(args[0]);
end;

function fasinh(const args : array of real) : real;
begin
   Result := arcsinh(args[0]);
end;

function facosh(const args : array of real) : real;
begin
   Result := arccosh(args[0]);
end;

function fatanh(const args : array of real) : real;
begin
   Result := arctanh(args[0]);
end;

function ffact(const args : array of real) : real;
var
   i : integer;
   res : real;
begin
   res := 1;
   for i := 1 to floor(args[0]) do
      res := res * i;
   Result := res;
end;

function fabs(const args : array of real) : real;
begin
   Result := abs(args[0]);
end;

function ffloor(const args : array of real) : real;
begin
   Result := floor(args[0]);
end;

function fceil(const args : array of real) : real;
begin
   Result := ceil(args[0]);
end;

function fsqrt(const args : array of real) : real;
begin
   Result := sqrt(args[0]);
end;
end.
