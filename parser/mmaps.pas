unit mMaps;

{$mode objfpc}{$H+}

interface
uses
   gmap, gutil, mFunction;

type
   TStringCompare = specialize TLess<string>;
   TFunctionMap = specialize TMap<string, TFunc, TStringCompare>;
   TVarMap = specialize TMap<string, real, TStringCompare>;

   PtrFunctionMap = ^TFunctionMap;
   PtrVarMap = ^TVarMap;
implementation
end.
