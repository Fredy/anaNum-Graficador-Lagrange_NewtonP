unit lagrangePol;

{$mode objfpc}{$H+}

interface

uses
   math;

type
   TArrofArrReal = array of array of real;

   TLagrangePol = class
   private
      pointsXY : TArrofArrReal;
      n : integer;
   public
      constructor create();

      function basisPol(i : integer; x : real) : real;
      function eval (x : real) : real;
      procedure setPointsXY(ptsXY : TArrofArrReal);
   end;

implementation

constructor TLagrangePol.create();
begin
   setLength(pointsXY, 0, 0);
   n := 0;
end;

procedure TLagrangePol.setPointsXY(ptsXY : TArrofArrReal);
begin
   pointsXY := ptsXY;
   n := length(ptsXY);
end;

function TLagrangePol.basisPol(i : integer; x : real) : real;
var
   j : integer;
   res : real;
begin
   res := 1;
   for j := 0 to n - 1 do
   begin
      if (j <> i) then
         res := res * ((x - pointsXY[j][0])
                       / (pointsXY[i][0] - pointsXY[j][0]));
   end;
   Result := res;
end;

function TLagrangePol.eval (x : real) : real;
var
   i : integer;
   res : real;
begin
   res := 0;
   for i := 0 to n - 1 do
      res := res + pointsXY[i][1]* self.basisPol(i, x);

   Result := res;
end;

end.
