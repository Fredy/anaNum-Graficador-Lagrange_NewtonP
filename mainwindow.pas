unit mainWindow;

{$mode objfpc}{$H+}

interface

uses
   Classes, SysUtils, FileUtil, TAGraph, TASeries, TAChartUtils, TAFuncSeries,
   TATransformations, Forms, Controls, Graphics, Dialogs, StdCtrls, ExtCtrls,
   ComCtrls, math, mCalculator, lagrangePol, newtonPol;

type

   { TformPlotter }

   TformPlotter = class(TForm)
      btnDraw: TButton;
      btnEval: TButton;
      Chart1: TChart;
      axisX: TConstantLine;
      axisY: TConstantLine;
      chkwoFunction: TCheckBox;
      chkPolynomial: TCheckBox;
      chkShowVals: TCheckBox;
      edtEvalValue: TEdit;
      pointsPolynom: TLineSeries;
      lblZoom: TLabel;
      plotFunction: TFuncSeries;
      plotPolynomial: TFuncSeries;
      plotFunctionManual: TLineSeries;
      plotPolynomialManual: TLineSeries;
      chkShowPts: TCheckBox;
      chkAutomaticPlot: TCheckBox;
      cbxMethods: TComboBox;
      edtEvalValues: TEdit;
      edtFunction: TEdit;
      lblEvalValues: TLabel;
      lblMethods: TLabel;
      lblFunction: TLabel;
      pnlGeneral: TPanel;
      tbrZoom: TTrackBar;
      procedure btnDrawClick(Sender: TObject);
      procedure btnEvalClick(Sender: TObject);
      procedure FormCreate(Sender: TObject);
      procedure FormDestroy(Sender: TObject);
      procedure plotFunctionCalculate(const AX: Double; out AY: Double);
      procedure plotPolynomialCalculate(const AX: Double; out AY: Double);
   private
      { private declarations }
      calculator : TCalculator;
      lagrange : TLagrangePol;
      newton : TNewtonPol;
      xEvalValues : array of real;
      evalValues : TArrofArrReal;
      function evalFunc(x : real) : real;
      procedure drawFunctionManual();
      procedure drawFunction();
      procedure drawPolynomialManual();
      procedure drawPolynomial();
      function calculateZoom() : real;
      procedure drawPoints();
   public
      { public declarations }
   end;

var
   formPlotter: TformPlotter;

implementation

{$R *.lfm}

{ TformPlotter }

procedure TformPlotter.btnDrawClick(Sender: TObject);
var
   tmpStrList : TStringList;
   i,j : integer;
begin
   if (edtFunction.GetTextLen = 0) then
   begin
      ShowMessage('Ingrese una función.');
      exit;
   end;

   calculator.solveExpression(edtFunction.text);

   if (edtEvalValues.GetTextLen = 0) then
   begin
      ShowMessage('Ingrese los valores a evaluar.');
      exit;
   end;

   tmpStrList := TStringList.create;

   extractStrings([','],[],PChar(edtEvalValues.text), tmpStrList);
   
   if not(chkwoFunction.Checked) then
   begin
      setLength(evalValues, tmpStrList.count, 2);
      setLength(xEvalValues, tmpStrList.count);
      for i := 0 to tmpStrList.count - 1 do
      begin
         xEvalValues[i] := strToFloat(tmpStrList[i]);
         evalValues[i][0] := strToFloat(tmpStrList[i]);
         evalValues[i][1] := self.evalFunc(evalValues[i][0]);
      end;
   end
   else
   begin
      setLength(evalValues, floor(tmpStrList.count / 2), 2);
      setLength(xEvalValues, floor(tmpStrList.count /2));
      //for i := 0 to floor(tmpStrList.Count / 2) - 1 do
         
         //evalValues[i][0] := strToFloat(tmpStrList[i]);
      i := 0;
      j := 0;
      while (i < tmpStrList.count) do
      begin
         xEvalValues[j] := strToFloat(tmpStrList[i]);
         evalValues[j][0] := strToFloat(tmpStrList[i]);
         evalValues[j][1] := strToFloat(tmpStrList[i + 1]);
         i := i +2;
         j := j + 1;
      end;    
   end;

   
   if (cbxMethods.ItemIndex = 0) then
      lagrange.setPointsXY(evalValues)
   else if (cbxMethods.ItemIndex = 1) then
      newton.setPointsXY(evalValues);



   if (chkAutomaticPlot.Checked) then
   begin
      if not(chkwoFunction.Checked) then
         self.drawFunctionManual()
      else
          plotFunctionManual.Active:= false;
      if (chkPolynomial.Checked) then
         self.drawPolynomialManual()
      else
         plotPolynomialManual.active := false;
   end
   else
   begin
      if not(chkwoFunction.Checked) then
         self.drawFunction()
      else
          plotFunction.Active:= false; 
      if (chkPolynomial.Checked) then
         self.drawPolynomial()
      else
         plotPolynomial.active := false;
   end;

   if (chkShowPts.Checked) then
      self.drawPoints()
   else
      pointsPolynom.clear();
end;

procedure TformPlotter.btnEvalClick(Sender: TObject);
var
   val : real;
begin
   
   if (edtEvalValue.GetTextLen = 0) then
   begin
      ShowMessage('Ingrese el valor a evaluar.');
      exit;
   end; 
   
   val := StrToFloat(edtEvalValue.Text);
   
   if (cbxMethods.ItemIndex = 0) then
      ShowMessage(FloatToStr(lagrange.eval(val)))
   else if (cbxMethods.ItemIndex = 1) then
      ShowMessage(FloatToStr(newton.eval(val)))      
   
end;

procedure TformPlotter.FormCreate(Sender: TObject);
begin
   calculator := TCalculator.create;
   calculator.solveExpression('x = 0');

   lagrange := TLagrangePol.create;
   newton := TNewtonPol.create;

   tbrZoom.Position := 1;
   cbxMethods.Items.Add('Lagrange');
   cbxMethods.Items.Add('Newton');
   cbxMethods.ItemIndex := 0;
end;

procedure TformPlotter.FormDestroy(Sender: TObject);
begin
   calculator.destroy;
end;

procedure TformPlotter.plotFunctionCalculate(const AX: Double; out AY: Double);
begin
   AY := evalFunc(AX);
end;

procedure TformPlotter.plotPolynomialCalculate(const AX: Double; out AY: Double
   );
begin
   if (cbxMethods.ItemIndex = 0) then
      AY := lagrange.eval(AX)
   else if (cbxMethods.ItemIndex = 1) then
      AY := newton.eval(AX);
end;

function TformPlotter.evalFunc(x: real): real;
begin
   Result := calculator.solveSavedExpression(['x'],[x]);
end;

procedure TformPlotter.drawFunction();
begin
   plotFunctionManual.active := false;
   plotFunction.active := false;

   plotFunction.extent.xMin := minvalue(xEvalValues) - self.calculateZoom();
   plotFunction.extent.xMax := maxvalue(xEvalValues) + self.calculateZoom();

   plotFunction.extent.useXMin := true;
   plotFunction.extent.useXMax := true;

   plotFunction.active := true;
end;

procedure TformPlotter.drawFunctionManual();
var
   xMin, xMax : real;
   h, newX : real;
begin
   plotFunction.active := false;

   plotFunctionManual.active := true;
   plotFunctionManual.clear();

   xMin := minvalue(xEvalValues) - self.calculateZoom();
   xMax := maxvalue(xEvalValues) + self.calculateZoom();
   h := abs((1 - xMin / xMax) / 100); // =  abs( ( Max - Min )/( 100 * Max ) );
   newX:= xMin;

   while newX < xMax do begin
      plotFunctionManual.AddXY(newX, self.evalFunc(newX));
      newX:= newX + h;
   end;

end;

procedure TformPlotter.drawPolynomial();
begin
   plotPolynomialManual.active := false;
   plotPolynomial.active := false;

   plotPolynomial.extent.xMin := minvalue(xEvalValues) - self.calculateZoom();
   plotPolynomial.extent.xMax := maxvalue(xEvalValues) + self.calculateZoom();

   plotPolynomial.extent.useXMin := true;
   plotPolynomial.extent.useXMax := true;

   plotPolynomial.active := true;
end;

procedure TformPlotter.drawPolynomialManual();
var
   xMin, xMax : real;
   h, newX : real;
begin
   plotPolynomial.active := false;

   plotPolynomialManual.active := true;
   plotPolynomialManual.clear();


   xMin := minvalue(xEvalValues) - self.calculateZoom();
   xMax := maxvalue(xEvalValues) + self.calculateZoom();
   h := abs((1 - xMin / xMax) / 100); // =  abs( ( Max - Min )/( 100 * Max ) );
   newX:= xMin;

   if (cbxMethods.ItemIndex = 0) then
   begin
      while newX < xMax do begin
         plotPolynomialManual.AddXY(newX, lagrange.eval(newX));
         newX:= newX + h;
      end;
   end
   else if (cbxMethods.ItemIndex = 1) then
   begin
      while newX < xMax do begin
         plotPolynomialManual.AddXY(newX, newton.eval(newX));
         newX:= newX + h;
      end;
   end




end;

procedure TformPlotter.drawPoints();
var
   i : integer;
begin
   pointsPolynom.clear();

   if (chkShowVals.Checked) then
      pointsPolynom.Marks.Style:= smsValue
   else
      pointsPolynom.Marks.Style:= smsNone;

   for i := 0 to length(evalValues) - 1 do
      pointsPolynom.AddXY(evalValues[i][0], evalValues[i][1]);
end;


function TformPlotter.calculateZoom() : real;
var
   h : real;
begin
   h := abs(maxvalue(xEvalValues) - minvalue(xEvalValues));
   h := h * 0.75 * tbrZoom.position;
   Result := h;
end;

end.
